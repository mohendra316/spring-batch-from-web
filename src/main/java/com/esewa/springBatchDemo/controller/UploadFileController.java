package com.esewa.springBatchDemo.controller;

import com.esewa.springBatchDemo.fileService.intf.UploadCsvToClassPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@Slf4j
public class UploadFileController {

    private final UploadCsvToClassPath uploadCsvToClassPath;
    private final JobLauncher jobLauncher;
    private final Job accountKeeperJob;

    @Value("${filePath}")
    private String filePath;


    @Autowired
    public UploadFileController(UploadCsvToClassPath uploadCsvToClassPath,
                                JobLauncher jobLauncher,
                                @Qualifier("accountJob")Job accountKeeperJob) {
        this.uploadCsvToClassPath = uploadCsvToClassPath;
        this.jobLauncher = jobLauncher;
        this.accountKeeperJob = accountKeeperJob;
    }

    @RequestMapping(value = "/run-batch-job", method = RequestMethod.POST)
    public String handle(@RequestParam("csvs") List<MultipartFile> file) throws Exception {
        log.info("Uploading files to classpath");
        uploadCsvToClassPath.uploadFileToCsv(file);
        log.info("Started batch job");
        JobParameters jobParameters = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .addString("inputFilePath", filePath)
                .toJobParameters();
        jobLauncher.run(accountKeeperJob, jobParameters);
        log.info("Ended batch job");
        return "Batch job invoked";
    }
}
