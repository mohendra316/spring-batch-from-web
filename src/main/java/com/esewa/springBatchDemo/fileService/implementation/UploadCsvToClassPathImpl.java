package com.esewa.springBatchDemo.fileService.implementation;

import com.esewa.springBatchDemo.fileService.intf.UploadCsvToClassPath;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class UploadCsvToClassPathImpl implements UploadCsvToClassPath {

    @Value("${filePath}")
    private String filePath;

    @Override
    public void uploadFileToCsv(List<MultipartFile> files) throws Exception {
        if (validateDocuments(files)) {
            for (MultipartFile file : files) {
                try {
                    PrintWriter writer = new PrintWriter("/"+filePath+"/"+file.getOriginalFilename(), "UTF-8");
                    String content = new String(file.getBytes());
                    writer.println(content);
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private Boolean validateDocuments(List<MultipartFile> documents) throws Exception {
        List<String> extensionList = new ArrayList<>();
        StringBuilder errorMessage = new StringBuilder();
        extensionList.add("csv");
        for (MultipartFile document : documents) {
            String extension = getMultipartDocumentExtension(document);
            if (extension == null || extension.isEmpty()) {
                log.error("Invalid file type");
                errorMessage.append(document.getOriginalFilename()).append(" : ").append("Invalid extension\n");
            }

            if (!document.getContentType().equals("text/csv") &&
                    !document.getContentType().equals("text/comma-separated-values") &&
                    !document.getContentType().equals("application/csv") &&
                    !document.getContentType().equals("application/vnd.ms-excel") &&
                    !document.getContentType().equals("application/vnd.msexcel")) {
                log.error("Invalid file");
                errorMessage.append(document.getOriginalFilename()).append(" : ").append("Invalid file type\n");
            }
            if (document.getSize() <= 0L) {
                log.error("Invalid file size");
                errorMessage.append(document.getOriginalFilename()).append(" : ").append("Invalid file size\n");
            }
            if (!extension.isEmpty() && !extensionList.contains(extension.toLowerCase())) {
                log.error("Invalid file format");
                errorMessage.append(document.getOriginalFilename()).append(" : ").append("Invalid file format\n");
            }
        }
        if (errorMessage.length() > 0) {
            throw new Exception(errorMessage.toString());
        }
        return true;
    }

    private String getMultipartDocumentExtension(MultipartFile file) {
        String userFileName = file.getOriginalFilename();
        if (userFileName.lastIndexOf(".") != -1 && userFileName.lastIndexOf(".") != 0) {
            return userFileName.substring(userFileName.lastIndexOf(".") + 1);
        }
        return null;
    }
}
