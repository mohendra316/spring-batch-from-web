package com.esewa.springBatchDemo.service;

import com.esewa.springBatchDemo.entity.Users;
import com.esewa.springBatchDemo.utils.ResourceUtil;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;


public class Reader extends MultiResourceItemReader {

    public Reader(String path) {
        super();
        initialize(path);

    }

    private void initialize(String path) {

        FlatFileItemReader<Users> reader = new FlatFileItemReader<Users>();
        reader.setLinesToSkip(1);
        reader.setResource(new ClassPathResource(path));
        BeanWrapperFieldSetMapper<Users> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Users.class);

        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[]{"userId", "name", "department", "account"});
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);

        DefaultLineMapper<Users> defaultLineMapper = new DefaultLineMapper<>();
        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        Resource[] resources = ResourceUtil.readResources(path, "Data");

        reader.setLineMapper(defaultLineMapper);
        setResources(resources);
        setDelegate(reader);
    }
}
