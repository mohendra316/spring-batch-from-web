package com.esewa.springBatchDemo.service;

import com.esewa.springBatchDemo.entity.Users;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CustomerValidator implements Validator<Users>{
    @Override
    public void validate(Users value) throws ValidationException {
        if (value.getName().equals("")) {
            log.info("Error in User with id : {}", value.getUserId());
            throw new ValidationException("Validation Exception");
        }
    }
}
